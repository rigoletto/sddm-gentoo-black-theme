### About

A simple SDDM theme inspired on FBSD SLiM theme.

### Preview!
![SDDM Gentoo Black Theme](https://gitlab.com/rigoletto/sddm-gentoo-black-theme/raw/master/src/screenshot.png)

### Installation
```shell
git clone https://gitlab.com/rigoletto/sddm-gentoo-black-theme.git
cp -R sddm-gentoo-black-theme/src /usr/share/sddm/themes/sddm-gentoo-black-theme
```

- Open up `/etc/sddm.conf` file and set `sddm-gentoo-black-theme` as your
  current theme.

```shell
[Theme]
# Current theme name
Current=sddm-gentoo-black-theme
```

### Configuration
- The theme uses the Liberation Sans font by default, but you can change it
  editing the `/usr/share/sddm/themes/sddm-gentoo-black-theme/theme.conf`
  file and setting the desired font in the `displayFont` variable.

```shell
[General]
background=background.png
displayFont="Liberation Sans"
```
